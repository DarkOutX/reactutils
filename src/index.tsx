
import * as React from "react";
import {render} from "react-dom";

import Utils from "./Utils";

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore

const rootEl = document.getElementById("root");

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
render(<Utils />, rootEl);
