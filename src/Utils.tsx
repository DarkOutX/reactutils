
import * as React from "react";
import {hot} from "react-hot-loader";
import { ISelectorItem } from "./components/Selector/consts";

import Selector from "./components/Selector/Selector";
import LoadingBG from "./components/LoadingBG/LoadingBG";

export interface IState {
    number: string;
}

export interface IProps {
    context?: any;
}

const testData: ISelectorItem[] = [
    {
        id: 1,
        name: "Spades",
        icon: "spades",
        classMod: "big",
    },
    {
        id: 2,
        name: "Diamonds",
        icon: "diamonds",
    },
    {
        id: 3,
        name: "Clubs",
        icon: "clubs",
        classMod: "tiny",
    },
    {
        id: 4,
        name: "Hearts",
        icon: "heart",
        classMod: "red",
    },
]

class Utils extends React.Component {

    protected constructor(props: IProps) {
        super(props);
    }

    render() {
        return <div> 
                    <div style={{display: "flex", width: "100%", justifyContent: "space-between", padding: 30, boxSizing: "border-box"}}>
                        <Selector placeholder={"Выберите опцию"} list={testData} style={{width: 250}}/>
                        <Selector list={testData} style={{width: 350}} disabled/>
                    </div>
                    <LoadingBG duration={8}>
                        <div style={{display: "flex", flexDirection: "column", width: 300, padding: 40}}>
                            <textarea></textarea>
                            <input/>
                            <button>Start</button>
                        </div>
                    </LoadingBG>
                </div>
    }
}

export default hot(module)(Utils);