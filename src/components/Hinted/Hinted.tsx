
import * as React from "react";
import { hot } from "react-hot-loader";
import Icon from "../Icon/Icon";

import './styles.scss';

interface IProps {
    hint: string;
    children: React.ReactNode;
}

export function Hinted(props: IProps) {
    const {
        hint,
    } = props;

    return (
        <span className='hinted' title={hint}>
            <span>{props.children}</span>
            <Icon className='hinted-icon' name='info' size={10} />
        </span>
    ); 
    //<Icon className='hinted-icon' name='info' size={10} />
}

export default hot(module)(Hinted);