
import "./styles.scss";
import React, {useEffect, useRef, useState} from "react";
import {type} from "os";

interface IMinItem {
    id: number | string;
}

interface IViewComponentProps {
    className?: string;
    item: IMinItem;
    addOnScrollFn?: (fn: Function) => void;
    isSelected?: boolean;
}

interface IProps {
    className?: string;
    items: IMinItem[];
    ViewComponent: React.FunctionComponent<IViewComponentProps>;
    onClick?: (selectedItemId?: number | void) => void;
    selectedId?: number | void;
    itemWidth?: number;
}

/**
 * Это вообще грязь и смешение DOM'а и React'а, но вроде бы рабочая оптимизация
 * Я по клику вешаю на элемент класс-модификатор 'selected' и сохраняю в эту переменную ссылку на DOM-элемент,
 *  чтобы при следующем клике переключить 'selected' с предыдущего элемент на новый
 *
 * Использование state'a здесь бы триггерило итерирование по всем элементам при каждом реселекте
 */
// let lastTarget: HTMLElement | void = void 0;

const ItemsTable: React.FunctionComponent<IProps> = (props) => {
    const {
        className: customClassName,
        items,
        ViewComponent,
        onClick: externalOnClick,
        selectedId,
        itemWidth = 300,
    } = props;

    let className = 'itemTable';
    const table = useRef<HTMLDivElement>(null);

    if (customClassName) {
        className += ` ${customClassName}`;
    }

    if (selectedId !== void 0) {
        className += ` smth_selected`;
    }

    const [ columnsAmount, setColumnsAmount ] = useState(Math.floor(window.innerWidth / itemWidth));

    const onResize = () => {
        const newColumnAmount = Math.floor(window.innerWidth / itemWidth);

        setColumnsAmount(newColumnAmount);
    }

    useEffect(() => {
        window.addEventListener('resize', onResize);

        return () => {
            window.removeEventListener('resize', onResize);
        };
    }, []);

    const scrollListeners: Function[] = [];

    function addScrollListener(fn: Function) {
        scrollListeners.push(fn);
    }

    const onScroll = () => {
        for (const fn of scrollListeners) {
            fn();
        }
    };

    const onClick = (event) => {
        const { target } = event;
        const idStr = target?.dataset.id;
        const id = idStr ? Number.parseInt(idStr, 10) : void 0;

        /*
        if (lastTarget) {
            lastTarget.classList.toggle('selected')
        }

        target.classList.toggle('selected');

        lastTarget = target;
        */

        if (typeof externalOnClick === 'function') {
            try {
                externalOnClick(id);
            }
            catch (err) {
                console.error(`[ItemsTable#onClick] Got an error while calling external onClick:\n`, err);
            }
        }
    };

    return (
        <div
            className={ className }
            onClick={ onClick }
            onScroll={ onScroll }
            ref={ table }
            data-columns={ columnsAmount }
            style={{
                gridTemplateColumns: `repeat(${columnsAmount}, minmax(0, 1fr))`,
            }}
        >
            { items.map(item => {
                const {
                    id,
                } = item;
                const isSelected = selectedId === id;

                return <ViewComponent
                    key={ id }
                    className={ `itemTable__item ${ isSelected ? 'selected' : ''}` }
                    item={ item }
                    addOnScrollFn={ addScrollListener }
                    isSelected = { isSelected }
                />;
            }) }
        </div>
    );
};

export default ItemsTable;
