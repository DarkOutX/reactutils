
import "./styles.scss";
import React from "react";
import Icon from "../Icon/Icon";

interface IProps {
    className?: string;
}

const Preloader: React.FunctionComponent<IProps> = (props) => {
    const {
        className: customClassName,
    } = props;
    let className = 'preloader';

    if (customClassName) {
        className += ` ${customClassName}`;
    }

    return (
        <div className={ className } >
            <Icon name={'spinner5'} />
        </div>
    );
};

export default Preloader;
