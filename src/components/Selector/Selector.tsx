
import * as React from "react";
import {hot} from "react-hot-loader";
import { ISelectorItem } from "./consts";

import Item from "./Item";

import "./Selector.scss";

export interface IState {
    isOpened: boolean;
    value?: ISelectorItem;
}

export interface IProps {
    style?: React.CSSProperties;
    placeholder?: string;
    list: ISelectorItem[];
    disabled?: boolean;
    value?: ISelectorItem;
    onChange?: Function; 
}

class Selector extends React.Component<IProps, IState> {

    state: IState = {
        isOpened: false,
    }

    private _selectorDOM: React.RefObject<HTMLDivElement> | undefined = React.createRef();
    private _listDOM: React.RefObject<any> | undefined = React.createRef();
    
    private __toggleList = this._toggleList.bind(this); 
    private __hideList = this._hideList.bind(this); 
    private __select = this._select.bind(this); 

    protected constructor(props: IProps) {
        super(props);
    }

    private _toggleList(event: any, force?: boolean) {
        const {
            disabled,
        } = this.props;
        
        if (disabled) {
            return;
        }

        this.setState({
            isOpened: (typeof force !== "undefined") ? force : !this.state.isOpened,
        });
    }

    private _select(item: ISelectorItem) {
        const {
            onChange: onChangeHandler,
        } = this.props;
        
        this.setState({
            isOpened: false,
            value: item,
        });
        
        if (typeof onChangeHandler === "function") {
            onChangeHandler(item);
        }
    }

    private _hideList() {
        this._toggleList(null, false);
    }

    componentDidMount() {
        if (this._selectorDOM) {
            this._selectorDOM.current?.addEventListener("focusout", this.__hideList);
        }
    }

    componentWillUnmount() {
        if (this._selectorDOM) {
            this._selectorDOM.current?.removeEventListener("focusout", this.__hideList);
        }
    }

    render() {
        const { 
            props: {
                list,
                placeholder,
                style,
                disabled,
            },
            state: {
                isOpened,
                value,
            },
            _selectorDOM,
            _listDOM,

            __toggleList: toggleList,
            __select: select,
        } = this;
        let placeholderData: ISelectorItem = {
            name: placeholder || list[0].name,
            icon: (placeholder) ? "" : list[0].icon,
            id: 0,
        };
        let selectorClassName = "selector";
        let listClassName = "selector-list";

        if (disabled) {
            selectorClassName += " disabled";
        }
        else if (isOpened) {
            selectorClassName += " opened";
        }

        if (value) {
            placeholderData = {
                name: value.name,
                icon: value.icon,
                id: value.id,
                classMod: value.classMod,
            };
        }

        // Show list above selector if it doesn't fit below
        if (isOpened && _selectorDOM && _selectorDOM.current && _listDOM && _listDOM.current) {
            const itemHeight = _selectorDOM.current.offsetHeight;
            const listHeight = itemHeight * list.length;
            const pageHeight = Math.max(
                document.body.scrollHeight, 
                document.body.offsetHeight, 
                document.documentElement.clientHeight, 
                document.documentElement.scrollHeight, 
                document.documentElement.offsetHeight, 
            );
            const freeSpaceBelow = pageHeight - _selectorDOM.current.getBoundingClientRect().top + itemHeight;

            if (freeSpaceBelow < listHeight) {
                listClassName += " top";
            }
        }

        return (
            <div 
                className={selectorClassName}
                tabIndex={0} // required for focusout event
                ref={_selectorDOM}
                style={style}
            >
                <div
                    className="selector-placeholder" 
                    onClick={toggleList}
                >
                    <Item data={placeholderData} ref={_listDOM} />
                    <span className="selector-placeholder__arrow">{(isOpened) ? "▲" : "▼"}</span>
                </div>
                <div className={listClassName}>    
                    {list.map(itemData => {
                        return <Item key={itemData.id} data={itemData} onClick={select}/>
                    })}
                </div>
            </div>)
    }
}

export default hot(module)(Selector);