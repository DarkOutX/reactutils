
import * as React from "react";
import {hot} from "react-hot-loader";
import { ISelectorItem } from "./consts";
import Icon from "./../Icon/Icon";

export interface IState {
    number: string;
}

export interface IProps {
    data: ISelectorItem,
    onClick?: any;
}

class Item extends React.Component<IProps, IState> {

    protected constructor(props: IProps) {
        super(props);
    }

    render() {
        const {
            props: {
                data: itemData,
                onClick: selectorClickHandler,
            },
        } = this;
        const {
            id,
            name,
            icon: iconName,
            classMod,
        } = itemData;
        let clickHandler = () => {};

        if (typeof selectorClickHandler === "function") {
            clickHandler = () => {selectorClickHandler(itemData)};
        }

        return <div className={(classMod) ? `item ${classMod}` : "item"} onClick={clickHandler}>
            <Icon name={iconName} />
            <span>{name}</span>
        </div>
    }
}

export default hot(module)(Item);