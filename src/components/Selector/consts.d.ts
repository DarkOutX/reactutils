export interface ISelectorItem {
    id: number,
    name: string,
    icon: string,
    classMod?: string, 
}