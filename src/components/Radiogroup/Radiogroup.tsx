
import * as React from "react";
import {hot} from "react-hot-loader";
import './styles.scss'

const {useRef, useState} = React; 

type AllowedValue = any;

export interface IRadioItem {
	label: string;
	value: AllowedValue;
}

interface IDOMBase {
	className?: string;
}

interface IInputBase extends IDOMBase {
	onChange?: (value: AllowedValue) => void;
	defaultValue?: AllowedValue;
}

interface IProps extends IInputBase {
	label?: string;
    items: IRadioItem[];
}

export function Radiogroup(props: IProps) {
    const {
    	onChange,
    	label: groupLabel,
    	items,
    	defaultValue = items[0].value,
    } = props;
	const [value, setValue] = useState<AllowedValue>(defaultValue);

    function onClick(event) {
    	let itemValue = event?.target?.value;
    	const numberItemValue = parseInt(itemValue, 10);

    	if (itemValue == numberItemValue) {
    		itemValue = numberItemValue;
    	}

    	if (itemValue !== value) {
    		setValue(itemValue);
    		
    		if (typeof onChange === 'function') {
	    		onChange(itemValue);
    		}
    	}
    }

    const DOMList = items.map((item, i) => {
        const {
        	label,
        	value: itemValue,
        } = item;
        const isSelected = itemValue === value;
        let className = isSelected ? 'selected' : '';

        return <label className={className} onClick={onClick} key={itemValue}>
        	<span>{label}</span>
        	<input type='radio' value={itemValue} radioGroup={groupLabel} name={groupLabel} defaultChecked={isSelected}/>
        </label>
    });

    return <div className='radiogroup'>
        <p>{groupLabel}</p>
        <div className='radiogroup__items'>
        	{DOMList}
        </div>
    </div>;
}

export default hot(module)(Radiogroup);
