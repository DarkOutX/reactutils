
import "./styles.scss";
import React, {useEffect, useRef, useState} from "react";
import Preloader from "../Preloader/Preloader";

const isDebug = false;

interface IProps {
    className?: string;
    src: string;
    isUsePreload?: boolean;
    addOnScrollFn?: (fn: Function) => void;
}

const ImageComponent: React.FunctionComponent<IProps> = (props) => {
    const {
        className: customClassName,
        src,
        isUsePreload = true,
        addOnScrollFn,
    } = props;
    let className = 'image';

    if (customClassName) {
        className += ` ${customClassName}`;
    }

    const isCheckViewport = typeof addOnScrollFn === 'function';
    const imgCont = useRef<HTMLDivElement>(null);
    const [ isLoaded, setIsLoaded ] = useState(!isUsePreload);
    const [ isInViewPort, setIsInViewPort ] = useState(!isCheckViewport);

    function isInViewport() {
        const rect = imgCont.current?.getBoundingClientRect();

        if (!rect) {
            return false;
        }

        if (isDebug) {
            console.log(
                'rect.top >= 0:', rect.top, '\n',
                'rect.left >= 0:', rect.left, '\n',
                'rect.bottom <= (***):', (rect.bottom <= (window.innerHeight || document.documentElement.clientHeight)), '\n',
                '  rect.bottom:', rect.bottom, '\n',
                '  window.innerHeight:', window.innerHeight, '\n',
                '  document.documentElement.clientHeight:', document.documentElement.clientHeight, '\n',
                'rect.right <= (***):', (rect.right <= (window.innerWidth || document.documentElement.clientWidth)), '\n',
                '  rect.right:', rect.right, '\n',
                '  window.innerWidth:', window.innerWidth, '\n',
                '  document.documentElement.clientWidth:', document.documentElement.clientWidth, '\n',
            )
        }

        return (
            rect.top <= window.innerHeight
            && rect.bottom >= 0
            && rect.left >= 0
            && rect.right <= (window.innerWidth || document.documentElement.clientWidth)
        );
    }

    if (typeof addOnScrollFn === 'function') {
        addOnScrollFn(() => {
            const isVisible = isInViewport();

            if (isVisible && isVisible !== isInViewPort) {
                setIsInViewPort(isVisible);
            }
        })
    }

    useEffect(() => {
        const img = imgCont.current;

        if (!img) {
            throw new Error(`[Image] Image container is not loaded`);
        }

        if (isCheckViewport) {
            setIsInViewPort(isInViewport());
        }

        if (!isUsePreload) {
            img.style.backgroundImage = `url('${src}')`;
        }
        else if (!isCheckViewport || (isCheckViewport && isInViewPort && !isLoaded)) {
            const imgHidden = new Image();

            imgHidden.src = src;

            if (isDebug) {
                console.count('Image loading')
            }

            imgHidden.onload = () => {
                img.style.backgroundImage = `url('${src}')`;

                setIsLoaded(true);
            }
        }
    }, [ isInViewPort ]);

    return (
        <div
            className={ className + (isLoaded ? '' : ' loading') }
            ref={ imgCont }
        >
            { !isLoaded ? <Preloader /> : void 0 }
        </div>
    );
};

export default ImageComponent;
