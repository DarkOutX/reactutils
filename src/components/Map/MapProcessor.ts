import Feature from "ol/Feature";
import Point from "ol/geom/Point";
import {fromLonLat, toLonLat} from "ol/proj";
import Vector from "ol/source/Vector";
import TileLayer from "ol/layer/Tile";
import XYZ from "ol/source/XYZ";
import VectorLayer from "ol/layer/Vector";
import {Translate} from "ol/interaction";
import olMap from "ol/Map";
import View from "ol/View";
import Zoom from "ol/control/Zoom";
import type {ILocation} from "tools/types/Locations";
import type {Geometry} from "ol/geom";
import Style from "ol/style/Style";
import * as olStyle from "ol/style";
import {MapBrowserEvent} from "ol";

interface IExternalListenersStore {
    onClick: ((featureId?: number | void) => void)[],
    onMove_loc: ((featureId: number, lonLat: [ lon: number, lat: number ]) => void)[],
}

class MapProcessor {

    locations: ILocation.Item[] = [];

    readonly control_toggleLocationsMove = false;

    private _map?: olMap;

    private _features_locs: Feature[] = [];
    private _featuresMap_locs: Record<number, Feature> = {};

    private _interaction_movableLocations?: Translate;
    private _btn_toggleLocationsMove?: HTMLButtonElement;

    private _isMovableFeatures = false;

    get isMovableFeatures() {
        return this._isMovableFeatures;
    }

    private _externalListeners: IExternalListenersStore = {
        onClick: [],
        onMove_loc: [],
    };

    readonly style_selectedPoint = new Style({
        image: new olStyle.Circle({
            radius: 10,
            fill: new olStyle.Fill({
                color: [ 247, 5, 25, .8 ],
            }),
            stroke: new olStyle.Stroke({
                color: [ 0, 0, 0, .4],
                width: 2,
            }),
        }),
    });

    private _selectedFeature?: Feature;
    private _selectedFeatureCoords_prev?: [ x: number, y: number ];

    get selectedFeature() {
        return this._selectedFeature;
    }

    get selectedFeatureCoordinates() {
        return this.getLocationCoordinates();
    }

    constructor() {

    }

    addListener(type: 'onClick', listener: (featureId?: number | void) => void)
    addListener(type: 'onMove_loc', listener: (featureId: number, lonLat: [ lon: number, lat: number ]) => void)
    addListener(type: keyof IExternalListenersStore, listener: Function) {
        const {
            _externalListeners,
        } = this;

        const thisEventListeners = _externalListeners[type];

        if (!thisEventListeners) {
            throw new Error(`[MapProcessor#addListener] Unknown event type: ${type}`);
        }

        if (typeof listener !== 'function') {
            throw new Error(`[MapProcessor#addListener] Provided listener for event ${type} must be function`);
        }

        if (thisEventListeners.some(existingListener => existingListener === listener)) {
            console.warn(`[MapProcessor#addListener] Trying to add duplicated listener for event ${type}`);

            return;
        }

        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        thisEventListeners.push(listener);
    }

    removeListener(type: string, listener: Function) {
        const {
            _externalListeners,
        } = this;

        const thisEventListeners = _externalListeners[type];

        if (!thisEventListeners) {
            throw new Error(`[MapProcessor#addListener] Unknown event type: ${type}`);
        }

        for (const [ i, savedListener ] of thisEventListeners.entries()) {
            if (savedListener === listener) {
                thisEventListeners.splice(i, 1);
            }
        }
    }

    selectLocation(locationId: number) {
        if (this._selectedFeature) {
            this._selectedFeature.setStyle(void 0);
        }

        const feature_loc = this._featuresMap_locs[locationId];

        this._selectFeature(feature_loc);
    }

    private _selectFeature(feature: Feature) {
        if (this._selectedFeature) {
            this._selectedFeature.setStyle(void 0);
        }

        feature.setStyle(this.style_selectedPoint);

        this._selectedFeature = feature;

        const featureCoordinates = this.getLocationCoordinates();

        if (!featureCoordinates) {
            throw new Error(`[MapProcessor#_selectFeature] Can't detect coordinates of selected feature`);
        }

        this._selectedFeatureCoords_prev = featureCoordinates;
    }

    getLocationCoordinates(id?: number): [ x: number, y: number ] | void {
        const feature = id ? this._featuresMap_locs[id] : this._selectedFeature;

        if (feature) {
            const featureGeometry = feature.get('geometry');

            return featureGeometry?.getCoordinates();
        }
    }

    resetSelectedLocationCoordinates() {
        const selectedFeature = this._selectedFeature;

        if (selectedFeature) {
            const featureGeometry = selectedFeature.get('geometry');

            featureGeometry.setCoordinates(this._selectedFeatureCoords_prev);
        }
    }

    unselectLocation() {
        // debugger;

        if (this._selectedFeature) {
            const selectedFeature = this._selectedFeature;

            selectedFeature.setStyle(void 0);
            this.resetSelectedLocationCoordinates();
        }

        this._selectedFeature = void 0;
    }

    setLocations(locations: ILocation.Item[]) {
        for (const location of locations) {
            const {
                id,
                coords: {
                    coordinates,
                },
            } = location;

            const feature_loc_point = new Feature({
                geometry: new Point(
                    fromLonLat(coordinates),
                ),
            });

            feature_loc_point.set('id', id);

            this._features_locs.push(feature_loc_point);
            this._featuresMap_locs[id] = feature_loc_point;
        }

        this.locations = [ ...locations ];
    }

    private _createBtn_toggleLocationsMove(className?: string): HTMLButtonElement {
        const toggleMoveBtn = document.createElement('button');

        toggleMoveBtn.className = `mapControls__toggleLocationsMove ${className ? className : ''}`;
        toggleMoveBtn.innerText = '<->';
        toggleMoveBtn.addEventListener('click', () => {
            this.toggleLocationsMove();
        });

        this._btn_toggleLocationsMove = toggleMoveBtn;

        return toggleMoveBtn;
    }

    toggleLocationsMove(forceValue?: boolean) {
        const {
            _map,
            _interaction_movableLocations,
            _btn_toggleLocationsMove,
            _isMovableFeatures,
        } = this;

        if (!_map) {
            throw new Error(`[MapProcessor#toggleLocationsMove] No map found`);
        }

        if (!_interaction_movableLocations) {
            throw new Error(`[MapProcessor#toggleLocationsMove] No translate interaction found`);
        }

        const value = forceValue !== void 0 ? forceValue : !_isMovableFeatures;

        if (value === true) {
            this._isMovableFeatures = true;
            _map.addInteraction(_interaction_movableLocations);

            if (_btn_toggleLocationsMove) {
                _btn_toggleLocationsMove.innerText = '<+>';
            }
        }
        else {
            this._isMovableFeatures = false;
            _map.removeInteraction(_interaction_movableLocations);

            if (_btn_toggleLocationsMove) {
                _btn_toggleLocationsMove.innerText = '<->';
            }
        }
    }

    private _createMapViewLayer(): TileLayer<XYZ> {
        return new TileLayer({
            source: new XYZ({
                url: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
            }),
        });
    }

    private _createLocationsLayer(): VectorLayer<Vector<Geometry>> {
        const vectorSource_locs = new Vector({ wrapX: false });

        vectorSource_locs.addFeatures(this._features_locs);

        return new VectorLayer({source: vectorSource_locs});
    }

    private _createInteraction_translate(layerToInteract: VectorLayer<Vector<Geometry>>) {
        const {
            _map,
        } = this;

        if (!_map) {
            throw new Error(`[MapProcessor#_createInteraction_translate] You must create map before move interaction`);
        }

        const translate = new Translate({
            layers: [ layerToInteract ],
        });

        translate.on('translatestart', e => {
            const feature = e.target.lastFeature_;
            const currentId = feature.get('id');
            const selectedId = this._selectedFeature?.get('id');
            const isSameFeature = currentId === selectedId;

            if (!isSameFeature) {
                this.resetSelectedLocationCoordinates();
                this._selectFeature(feature);
                this._onMapClick_externals(currentId);
            }
            /*
            else {
                console.log('Translate start on same location');
            }
            */
        });

        translate.on('translateend', event => {
            const {
                target,
                startCoordinate,
                coordinate: endCoordinate,
            } = event;

            const isNoMoved = startCoordinate[0] === endCoordinate[0] && startCoordinate[1] === endCoordinate[1];

            if (isNoMoved) {
                return;
            }

            const lonLat = toLonLat(endCoordinate) as [ lon: number, lat: number ];
            const feature = target.lastFeature_;
            const id = feature.get('id');

            this._onLocationMove_externals(id, lonLat);
        });

        this._interaction_movableLocations = translate;
    }

    private _onMapClick_externals(featureId?: number | void) {
        const {
            _externalListeners: {
                onClick: onClickFns,
            },
        } = this;

        for (const onClickFn of onClickFns) {
            if (typeof onClickFn === 'function') {
                try {
                    onClickFn(featureId);
                }
                catch (err) {
                    console.error(`[Map#onClick] Got an error while calling external onClick:\n`, err);
                }
            }
        }
    }

    private _onLocationMove_externals(featureId: number, lonLat: [ lon: number, lat: number ]) {
        const {
            _externalListeners: {
                onMove_loc: onMove_locFns,
            },
        } = this;

        for (const onLocationMoveFn of onMove_locFns) {
            if (typeof onLocationMoveFn === 'function') {
                try {
                    onLocationMoveFn(featureId, lonLat);
                }
                catch (err) {
                    console.error(`[Map#onLocationMove] Got an error while calling external onLocationMove:\n`, err);
                }
            }
        }
    }

    private _onMapClick = (event: MapBrowserEvent<any>) => {
        const {
            _map,
            _isMovableFeatures,
        } = this;

        if (!_map) {
            throw new Error(`[MapProcessor#_onMapClick] Map is not defined`);
        }

        const feature = _map.forEachFeatureAtPixel(event.pixel, (feat/*, layer*/) => feat) as (Feature | void);

        let id: number | void = void 0;

        if (feature) {
            // if _isMovableFeatures === true, selection will be handled in .on('translatestart')
            if (_isMovableFeatures) {
                return;
            }

            const isSameFeature = feature.get('id') === this._selectedFeature?.get('id');

            if (!isSameFeature) {
                id = feature.get('id');

                this.unselectLocation();
                this._selectFeature(feature);
            }
        }
        else {
            this.unselectLocation();
        }

        this._onMapClick_externals(id);
    }

    createMap(container: HTMLElement, options: { center?: [ lon: number, lat: number ], startZoom?: number } = {}) {
        const {
            locations,
            _map,
        } = this;

        if (!Array.isArray(locations) || locations.length === 0) {
            throw new Error(`[MapProcessor#createMap] Not found any locations`);
        }

        if (_map) {
            throw new Error(`[MapProcessor#createMap] Map is already created. You should call destroyMap() first`);
        }

        const {
            center,
            startZoom,
        } = options;

        const layer_map = this._createMapViewLayer();
        const layer_locs = this._createLocationsLayer();

        const map = new olMap({
            target: container,
            layers: [
                layer_map,
                layer_locs,
            ],
            controls: [  ],
            view: new View({
                center: center ? fromLonLat(center) : [ 0, 0 ],
                zoom: startZoom || 0,
            }),
        });

        map.on('click', this._onMapClick);

        this._map = map;

        this._createInteraction_translate(layer_locs);
    }

    createControls(container: HTMLElement) {
        const {
            control_toggleLocationsMove,
            _map,
            _btn_toggleLocationsMove,
        } = this;

        if (!_map) {
            throw new Error(`[MapProcessor#createControls] No map defined`);
        }

        _map.addControl(new Zoom({ target: container }));

        // @ts-ignore
        const controlsDOM: HTMLButtonElement[] = [ ...container.children ];

        for (const controlDOM of controlsDOM) {
            const className = controlDOM.classList[0];

            switch (className) {
                case 'ol-zoom': {

                    const zoomIn_btn: HTMLButtonElement = controlDOM.children[0] as HTMLButtonElement;
                    const zoomOut_btn: HTMLButtonElement = controlDOM.children[1] as HTMLButtonElement;

                    zoomIn_btn.dataset.type = 'icon';
                    zoomIn_btn.classList.add('f-icon-zoom-in');
                    zoomIn_btn.classList.add('ol-control-custom');
                    zoomIn_btn.innerText = '';

                    zoomOut_btn.dataset.type = 'icon';
                    zoomOut_btn.classList.add('f-icon-zoom-out');
                    zoomOut_btn.classList.add('ol-control-custom');
                    zoomOut_btn.innerText = '';

                    break;
                }
            }
        }

        if (control_toggleLocationsMove) {
            let btn_translateMove: HTMLButtonElement | void = _btn_toggleLocationsMove;

            if (!_btn_toggleLocationsMove) {
                btn_translateMove = this._createBtn_toggleLocationsMove();
            }

            container.appendChild(btn_translateMove as HTMLButtonElement);
        }
    }
}

export default new MapProcessor();
