
import "./Map.scss";
import * as React from "react";
import mapProcessor from "./MapProcessor";
import type { ILocation } from "tools/types/Locations";

interface ILocation_min {
    id: number;
    coords: { coordinates: [ number, number ] };
}

interface IProps {
    className?: string;
    locations: ILocation.Item[];
    selectedLocationId?: number | void;
    onClick?: (selectedLocation?: number | void) => void;
    onMoveLocation?: (selectedLocation: number | void, newCoords: [ lon: number, lat: number ]) => void;
}

const position: [ number, number ] = [ 29.770077596390646, 59.99060488562674 ];

const Map: React.FunctionComponent<IProps> = (props) => {
    const {
        className: customClassName,
        locations,
        onClick,
        selectedLocationId,
        onMoveLocation,
    } = props;
    let className = 'mapContainer';

    if (customClassName) {
        className += ` ${customClassName}`;
    }

    const mapCont = React.useRef<HTMLDivElement | null>(null);
    const mapControlsCont = React.useRef<HTMLDivElement | null>(null);
    let isMapRendered = false;

    React.useEffect(() => {
        if (selectedLocationId) {
            mapProcessor.selectLocation(selectedLocationId);
        }
        else {
            mapProcessor.unselectLocation();
        }
    }, [ selectedLocationId ]);

    React.useEffect(() => {
        if (!isMapRendered && mapCont.current && mapControlsCont.current) {
            mapProcessor.setLocations(locations);
            mapProcessor.createMap(mapCont.current, { center: position, startZoom: 14 });
            mapProcessor.createControls(mapControlsCont.current);

            if (onClick) {
                mapProcessor.addListener("onClick", onClick);
            }

            if (onMoveLocation) {
                mapProcessor.addListener("onMove_loc", onMoveLocation);
            }

            isMapRendered = true;

            console.debug('Map rendered');
        }
    }, []);

    return (
        <div className={ className }>
            <div
                className={ 'mapContainer__map' }
                // style={ {width: '800px', height: '500px'} }
                ref={mapCont}
            >
            </div>
            <div
                className={ 'mapContainer__controls' }
                // style={ {
                //     width: '100px',
                //     height: '100px',
                // } }
                ref={mapControlsCont}
            >
            </div>
        </div>
    );
};

export default Map;
