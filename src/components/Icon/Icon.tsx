
import "./styles.scss";
import * as React from "react";

// Styles
import "./font-icon/font-styles.scss";

type IconProps = {
    name: string;
    size?: number;
    name2?: string;
    size2?: number;
    position2?: 'topLeft' | 'bottomLeft' | 'topRight' | 'bottomRight' | 'overflow';
    className?: string;
    hint?: string;
};

export default class Icon extends React.Component<IconProps, Record<string, unknown>> {
    public render() {
        const {
            props: {
                name,
                size = 14,
                name2,
                size2,
                position2 = 'topRight',
                className: customClassName,
                hint,
            },
        } = this;
        const baseClassName = 'icon';

        let className = baseClassName;

        if (customClassName) {
            className += ` ${customClassName}`;
        }

        const sizeInPt = size + 'pt';
        const subSizeInPt = (size2 ?? (size / 2.5)) + 'pt';
        const icon2 = name2
            ? <span
                className={" f-icon-" + name2 + ` subIcon ${position2}`}
                data-type={"icon"}
                style={{ fontSize: subSizeInPt }}
                title={hint}
            />
            : void 0
        ;
        const style = {
            fontSize: sizeInPt,
            width: sizeInPt,
            height: sizeInPt,
        };

        if (icon2) {
            className += ' double';
        }

        return (
            <span
                className={className + " f-icon-" + name}
                data-type={"icon"}
                style={ style }
                title={hint}
            >
                { icon2 }
            </span>
        );
    }
}
