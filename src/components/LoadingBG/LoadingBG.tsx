
import * as React from "react";
import {hot} from "react-hot-loader";

import  "./LoadingBG.scss";

export interface IState {
    number: string;
}

export interface IProps {
    duration?: number;
    background?: string,
    style?: React.CSSProperties,
}

const DEFAULT_BACKGROUND = `linear-gradient(90deg, rgba(36,131,23,1) 0%, rgba(17,205,35,0.7847514005602241) 100%, rgba(59,59,59,0) 100%)`;
const DEFAILT_DURATION = 4;

class LoadingBG extends React.Component<IProps, IState> {

    protected constructor(props: IProps) {
        super(props);
    }

    render() {
        const {
            props: {
                duration = DEFAILT_DURATION,
                background = DEFAULT_BACKGROUND,
                style = {},
            },
        } = this;
        const animationDuration = (duration).toString() + "s";

        if (!style.position) {
            style.position = "relative";
        }

        return <div style={style}>
                <div className="loading-bg" style={{animationDuration, background}}></div>
                {this.props.children}
            </div>
    }
}

export default hot(module)(LoadingBG);