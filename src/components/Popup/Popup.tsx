/* eslint-disable no-mixed-spaces-and-tabs,indent */
import * as React from "react";
import './styles.scss'

const {useRef, useState} = React;

interface IDOMBase {
	className?: string;
}

interface IProps extends IDOMBase {
	showCloseBtn?: boolean;
    bgOpacity?: number;
	showBg?: boolean;
	isCloseByBg?: boolean;
	isClosable?: boolean;
	onClose?: () => void;
	children: React.ReactNode;
}

export default function Popup(props: IProps) {
    const {
    	showBg = true,
    	isClosable = true,
    	className,
    	onClose,
        bgOpacity,
    } = props;
    let {
    	showCloseBtn = isClosable,
		isCloseByBg = isClosable,
    } = props;

    if (!isClosable) {
    	showCloseBtn = false;
    	isCloseByBg = false;
    }

    function _onClose(ev) {
    	if (ev.target !== ev.currentTarget) {
    		return;
    	}

    	if (typeof onClose === 'function') {
    		onClose();
    	}
    }

    const bgStyle: React.CSSProperties = {
    	opacity: showBg ? (bgOpacity ? bgOpacity : 1) : 0,
    	pointerEvents: showBg && isCloseByBg ? 'all' : 'none',
    	cursor: showBg ? isCloseByBg ? 'pointer' : 'not-allowed' : 'auto',
    };

    const popupStyle: React.CSSProperties = {
    	pointerEvents: 'visible',
    	cursor: 'auto',
    };

    return <div className='popup' style={popupStyle}>
        {showCloseBtn
            ? <div
                className='popup__close-btn'
                onClick={_onClose}
            >
                x
            </div>
            : void 0
        }
        <div className='popup-bg' style={bgStyle} onClick={(ev) => {isCloseByBg ? _onClose(ev) : void 0}} />
        {props.children}
    </div>;
}
